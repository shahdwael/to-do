<html>
<head>
    <?php if (isset($title)): ?>
            <title>To-do: <?= $title ?></title>
        <?php else: ?>
            <title>To-do</title>
        <?php endif ?>
    </head>
    <body>
        <div>
        <?php if (!empty($_SESSION["id"])): ?>
                    <ul>
                        <li><a href="/">Categories</a></li>
                        <li><a href="/createCategory.php">Create Category</a></li>
                        <li><a href="/createList.php">Create List</a></li>
                        <li><a href="/createTodo.php">Create To-do item</a></li>
                        <li><a href="/logout.php"><strong>Log Out</strong></a></li>
                    </ul>
                <?php endif ?>

            </div>
    