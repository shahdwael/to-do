<?php

require ("../includes/config.php");

if($_SERVER["REQUEST_METHOD"]=="GET"){
    render("login.php",["title"=>"Login"]);
    
}
else if ($_SERVER["REQUEST_METHOD"]=="POST"){
    $username= $_POST["username"];
    $password=$_POST["password"];
    
    if(empty($username)){
        apologize("Username is empty!");
    }
    else if(empty($password)){
        apologize("Password is empty!");
    }
    else {
        $rows=query("SELECT * FROM user WHERE Username='$username'");
        if (count($rows)==1){
            $row=$rows[0];
            if (password_verify($password,$row["Hash"])){
                $_SESSION["id"]= $row["ID"];
                redirect("/");
            }
        }
        apologize("Invalid username and/or password.");
    }
}

?>