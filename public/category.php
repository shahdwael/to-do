<?php

require ("../includes/config.php");

$categoryId=$_GET['categoryId'];

if(empty($categoryId)){
    apologize("Please choose the category");
}
else{
    $userID= $_SESSION["id"];
    
    $category=query("SELECT * FROM catergory WHERE ID=$categoryId AND User_ID=$userID");
    $category=$category[0];
    
    $lists=query("SELECT * FROM list WHERE category_ID=$categoryId");
    
    render("category.php",["title"=>"Category ". $category["Name"], "category"=> $category, "lists"=> $lists ]);
}

?>