<?php

require ("../includes/config.php");

$listId=$_GET['listId'];

if(empty($listId)){
    apologize("Please choose the list");
}
else{
    $userID= $_SESSION["id"];
    
    $list=query("SELECT * FROM list WHERE ID=$listId");
    $list=$list[0];
    
    $items=query("SELECT * FROM items WHERE list_ID=$listId");
    
    
    render("list.php",["title"=>"List ". $list["Name"], "list"=> $list, "items"=>$items ]);
}

?>